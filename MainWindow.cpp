#include <iostream>
#include <QTableView>
#include <QBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QStackedWidget>
#include "MainWindow.h"
#include "TextParser.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QBoxLayout *pBoxLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
    QVBoxLayout *pLeftLayout = new QVBoxLayout();
    QVBoxLayout *pRightLayout = new QVBoxLayout();

    pBoxLayout->setAlignment(Qt::AlignTop);
    pLeftLayout->setAlignment(Qt::AlignHCenter);
    pRightLayout->setAlignment(Qt::AlignHCenter);

    pBoxLayout->addLayout(pLeftLayout);
    pBoxLayout->addLayout(pRightLayout);

    QStackedWidget *pViewStack = new QStackedWidget();

    TextParser parser;
    mRecipeList =
        parser.parse("/home/andrew/Development/cookbook/recipes.txt");
    bool left = true;
    for (auto it = mRecipeList.begin(); it != mRecipeList.end(); ++it)
    {
        //pBoxLayout->addWidget(*it);
        QWidget *pTileWidget = (*it)->getRecipeTile();
        (*it)->setCallback([pViewStack, it](){
                pViewStack->setCurrentIndex(0);
                pViewStack->removeWidget(*it);
                });
        connect((QPushButton*)pTileWidget, &QPushButton::pressed, [this,it, pViewStack](){

                int pos = pViewStack->addWidget(*it);
                pViewStack->setCurrentIndex(pos);
                });
        if (left)
            pLeftLayout->addWidget(pTileWidget);
        else
            pRightLayout->addWidget(pTileWidget);
        left = !left;
    }

    if (!left)
    {
        QWidget *pFillerWidget = new QWidget();
        pFillerWidget->setFixedSize(TILE_WIDTH, TILE_HEIGHT);
        pRightLayout->addWidget(pFillerWidget);
    }


    QScrollArea *pScrollArea = new QScrollArea(this);
    pViewStack->addWidget(pScrollArea);
    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(pBoxLayout);
    pScrollArea->setWidget(centralWidget);
    setCentralWidget(pViewStack);

    setFixedSize(1024, 600);
}

MainWindow::~MainWindow()
{
}
