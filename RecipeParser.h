#ifndef RECIPE_PARSER_H
#define RECIPE_PARSER_H

#include <QObject>
#include <list>
#include "Recipe.h"

class RecipeParser
{
public:
    explicit RecipeParser();
    ~RecipeParser();

    virtual std::list<Recipe*> parse(const std::string &filePath) = 0;
    
private:
};

#endif // RECIPE_PARSER_H

