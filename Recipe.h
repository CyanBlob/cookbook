#ifndef RECIPE_H
#define RECIPE_H

#include <QLabel>
#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <functional>

const int TEXT_MARGIN = 0;
const int WIDTH = 987;
const int IMAGE_HEIGHT = 200;
const int TILE_WIDTH = 491;
const int TILE_HEIGHT = 491;
const int BUTTON_WIDTH = 100;

struct RecipeInfo
{
    std::string title;
    std::string description;
    std::string photoUrl;
    int prepTime;
    int cookTime;
    std::list<std::string> ingredients;
    std::list<std::string> directions;
};

class Recipe : public QScrollArea
{
public:
    explicit Recipe(RecipeInfo &recipeInfo, QObject *parent = 0);
    ~Recipe();
    QWidget* getRecipeTile();
    void setCallback(std::function<void()> callback){mCallback = callback;};
private:
    RecipeInfo mRecipeInfo;
    QLabel *mpTileImageLabel;
    QImage *mpTileImage;
    std::function<void()> mCallback;
};
#endif // RECIPE_H
