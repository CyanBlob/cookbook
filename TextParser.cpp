#include <iostream>
#include <fstream>
#include "TextParser.h"

const std::string &TITLE_TAG = "Title: ";
const std::string &PHOTO_URL_TAG = "Photo Url: ";
const std::string &INGREDIENTS_TAG = "Ingredients:";
const std::string &DIRECTIONS_TAG = "Directions:";

enum TagType {
    Title,
    Description,
    PhotoUrl,
    PrepTime,
    CookTime,
    Ingredients,
    Directions,
    None
};

TextParser::TextParser()
{
}

TextParser::~TextParser()
{
}

std::list<Recipe*> TextParser::parse(const std::string &filePath)
{
    std::cout<<"PARSING: "<<filePath<<std::endl;

    std::list<Recipe*> recipes;
    std::ifstream file(filePath);
    std::string line;

    TagType currentTag = None;
    RecipeInfo recipeInfo;
    while (file)
    {
        std::getline(file, line);

        if (line.length() == 0)
        {
            currentTag = None;
        }

        if (line.find(TITLE_TAG) != std::string::npos)
        {
            currentTag = Title;
            if (recipeInfo.title.length() > 0)
            {
                recipes.push_back(new Recipe(recipeInfo));
            }
            recipeInfo = {};
            // TODO: strip extra whitespace
            recipeInfo.title = line.substr(TITLE_TAG.length());
        }
        else if (line.find(PHOTO_URL_TAG) != std::string::npos)
        {
            currentTag = PhotoUrl;
            recipeInfo.photoUrl = line.substr(PHOTO_URL_TAG.length());
        }
        else if (line.find(INGREDIENTS_TAG) != std::string::npos)
        {
            currentTag = Ingredients;
            recipeInfo.ingredients.push_back(INGREDIENTS_TAG);
        }
        else if (line.find(DIRECTIONS_TAG) != std::string::npos)
        {
            currentTag = Directions;
            recipeInfo.directions.push_back(DIRECTIONS_TAG);
        }
        else if (currentTag == Ingredients)
        {
            recipeInfo.ingredients.push_back(line);
        }
        else if (currentTag == Directions)
        {
            recipeInfo.directions.push_back(line);
        }
    }
    file.close();
    return recipes;
}
