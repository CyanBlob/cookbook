#ifndef TEXT_PARSER_H
#define TEXT_PARSER_H

#include "RecipeParser.h"

class TextParser : public RecipeParser
{
public:
    explicit TextParser();
    ~TextParser();

    std::list<Recipe*> parse(const std::string &filePath);
    
private:
};

#endif // TEXT_PARSER_H

