#include <QUrl>
#include <QPushButton>
#include <QHBoxLayout>
#include <QImageReader>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QPixmap>
#include <QFont>
#include <iostream>
#include "Recipe.h"

Recipe::Recipe(RecipeInfo &recipeInfo, QObject *parent)
    : QScrollArea()
{
    mRecipeInfo = recipeInfo;

    QWidget *pMainWidget = new QWidget();
    QVBoxLayout *pMainLayout = new QVBoxLayout();
    pMainWidget->setLayout(pMainLayout);
    this->setWidget(pMainWidget);
    this->setWidgetResizable(true);
    // TODO: figure out this weird sizing
    this->setFixedWidth(WIDTH + (1024 - WIDTH) + 1);

    QHBoxLayout *pTitleBarLayout = new QHBoxLayout();

    QPushButton *pCloseButton = new QPushButton(QString::fromStdString("Back"));
    pCloseButton->setFocusPolicy(Qt::NoFocus);
    pCloseButton->setFixedWidth(BUTTON_WIDTH);
    pCloseButton->setContentsMargins(0, 0, 0, 0);
    connect (pCloseButton, &QPushButton::pressed, [this](){
            mCallback();
            });
    pTitleBarLayout->addWidget(pCloseButton);

    QLabel *mpTitleLabel = new QLabel(QString::fromStdString(recipeInfo.title));
    // TODO: why do I need -15?
    mpTitleLabel->setFixedWidth(WIDTH - BUTTON_WIDTH * 2 - 15);

    QFont font(mpTitleLabel->font());
    font.setPointSize(24);
    mpTitleLabel->setFont(font);
    mpTitleLabel->setAlignment(Qt::AlignHCenter);
    mpTitleLabel->setContentsMargins(0, 0, 0, 0);
    pTitleBarLayout->addWidget(mpTitleLabel);

    QWidget *pTitleBarSpacer = new QWidget();
    pTitleBarSpacer->setFixedWidth(BUTTON_WIDTH);
    pTitleBarSpacer->setContentsMargins(0, 0, 0, 0);
    pTitleBarLayout->addWidget(pTitleBarSpacer);

    pMainLayout->addLayout(pTitleBarLayout);

    QLabel *pImageLabel = new QLabel();
    QImage *pImage = new QImage(WIDTH, IMAGE_HEIGHT, QImage::Format_RGBA8888);
    pImageLabel->setPixmap(QPixmap::fromImage(*pImage));
    pMainLayout->addWidget(pImageLabel);

    mpTileImageLabel = new QLabel();
    mpTileImage = new QImage(TILE_WIDTH, TILE_HEIGHT, QImage::Format_RGBA8888);
    mpTileImageLabel->setPixmap(QPixmap::fromImage(*mpTileImage));

    QHBoxLayout *pHBoxLayout = new QHBoxLayout();
    pMainLayout->addLayout(pHBoxLayout);

    QVBoxLayout *pIngredientsLayout = new QVBoxLayout();
    QVBoxLayout *pDirectionsLayout = new QVBoxLayout();
    pIngredientsLayout->setAlignment(Qt::AlignTop);
    pDirectionsLayout->setAlignment(Qt::AlignTop);

    pHBoxLayout->addLayout(pIngredientsLayout);
    pHBoxLayout->addLayout(pDirectionsLayout);

    for (auto it = recipeInfo.ingredients.begin();
            it != recipeInfo.ingredients.end(); ++it)
    {
        QLabel *pIngredientLabel = new QLabel(QString::fromStdString(*it));
        pIngredientLabel->setWordWrap(true);
        pIngredientLabel->setMargin(TEXT_MARGIN);
        pIngredientsLayout->addWidget(pIngredientLabel);
    }
    for (auto it = recipeInfo.directions.begin();
            it != recipeInfo.directions.end(); ++it)
    {
        QLabel *pDirectionsLabel = new QLabel(QString::fromStdString(*it));
        pDirectionsLabel->setWordWrap(true);
        pDirectionsLabel->setMargin(TEXT_MARGIN);
        pDirectionsLayout->addWidget(pDirectionsLabel);
    }

    QNetworkAccessManager *pQnam = new QNetworkAccessManager(this);

    connect(pQnam, &QNetworkAccessManager::finished,
        ([this, pImageLabel](QNetworkReply* pReply){

                if (pReply->error() == QNetworkReply::NoError)
                {
                    QImageReader imageReader(pReply);
                    imageReader.setAutoDetectImageFormat(true);
                    QImage pic = imageReader.read();
                    pImageLabel->setPixmap(QPixmap::fromImage(pic.scaledToWidth(WIDTH)));
                    mpTileImageLabel->setPixmap(QPixmap::fromImage(pic.scaledToHeight(TILE_HEIGHT)));
                }
            }));

    if (recipeInfo.photoUrl.length() > 0)
    {
        QUrl url(QString::fromStdString(recipeInfo.photoUrl));
        pQnam->get(QNetworkRequest(url));
    }
    else
    {
        QUrl url("https://25wxih3lxatn2okzkn1cr69o-wpengine.netdna-ssl.com/wp-content/uploads/2016/01/generic-recipe-1024x580.jpg");
        pQnam->get(QNetworkRequest(url));
    }
}

QWidget* Recipe::getRecipeTile()
{
    QPushButton *pTileWidget = new QPushButton();
    pTileWidget->setFocusPolicy(Qt::NoFocus);
    pTileWidget->setFixedHeight(TILE_HEIGHT);
    pTileWidget->setFixedWidth(TILE_WIDTH);

    QVBoxLayout *pVBoxLayout = new QVBoxLayout();
    pVBoxLayout->setAlignment(Qt::AlignHCenter);

    QLabel *pTitle = new QLabel(QString::fromStdString(mRecipeInfo.title));
    pTitle->setAlignment(Qt::AlignCenter);
    pVBoxLayout->addWidget(pTitle);

    pTileWidget->setLayout(pVBoxLayout);
    pVBoxLayout->addWidget(mpTileImageLabel);

    return pTileWidget;
}

Recipe::~Recipe()
{
}
