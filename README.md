# cookbook

A Qt-based cookbook, intended for use on a dedicated Raspberry Pi

# cross-compiling
In order to cross-compile for the Raspberry Pi, follow the instructions here: https://wiki.qt.io/RaspberryPi2EGLFS

NOTE: In the part where we setup the symlinks on the pi to the EGL and GLES libs, I had to change the order in order to avoid broken symlinks

# on the Raspberry Pi
After following the above instructions, I was getting the following error:
'qt.qpa.plugin: Could not find the Qt platform plugin "eglfs" in ""
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.'

To fix this, setup the following environment variables:

QT_PLUGIN_PATH=/usr/local/qt5pi/plugins:/usr/local/qt5pi/plugins/platforms

LD_LIBRARY_PATH=/usr/local/qt5pi/lib

After that, if you get the error "failed to open vchiq instance", change the permissions on /dev/vchiq. For example, with the command: sudo chmod 777 /dev/vchiq

Once everything is up and running, you may need to change the permissions on /dev/input/event* to ensure that the user running the software can read them
